export let contactsArray = [
  {
    "name"      : "Apple Inc.",
    "work"      : `1 Infinite Loop
                  Cupertino CA 95014
                  United States`,
    "main"      : "1800MYAPPLE",
    "src"       : "./images/apple-icon-21.png",
    "home_page" : "https://www.apple.com",
    "note"      : "",
    "groups"    : ['A1', 'C1']
  },
  {
    "name"      : "Monika",
    "note"      : "",
    "groups"    : ['A1', 'C1']
  }
];

export let groupsArray = [
  {
    "groups"    : [
                    {
                      "name"  :"All Contacts",
                      "id"    : 'A1'
                    }
                  ],
    "type"      : "Section",
    "id"        : 'A'
  }, 
  {
    "header"    : "Google",
    "groups"    : [
                    {
                      "name"  :"All Google",
                      "id"    : 'B1'
                    }
                  ],
    "type"      : "Section",
    "id"        : 'B'
  },
  {
    "header"    : "On My Mac",
    "groups"    : [
                    {
                      "name"  :"All on My Mac",
                      "id"    : 'C1'
                    }
                  ],
    "type"      : "Section",
    "id"        : 'C'
  },
  {
    "header"    : "Directories",
    "groups"    : [
                    {
                      "name"  :"All Directories",
                      "id"    : 'D1'
                    },
                    {
                      "name"  :"Directory Services",
                      "id"    : 'D2'
                    },
                    {
                      "name"  :"Google",
                      "id"    : 'D3'
                    }
                  ],
    "type"      : "Section",
    "id"        : 'D'
  }
];