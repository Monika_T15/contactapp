import React from 'react';
import {EditContact} from './EditContact';
import { toggleClasses } from '../utils/toggle-classes';
import { editContact } from '../actions';

export function onEditContact () {
  let table = document.getElementById('table');
  let cells = table.getElementsByTagName('td');
  let editButton = document.getElementById('edit');
  let doneButton = document.getElementById('done');
  let contenteditable;
  for (let i=0,len=cells.length; i<len; i++){
    contenteditable = !cells[i].isContentEditable;
    cells[i].setAttribute('contenteditable', !cells[i].isContentEditable);
  }
  toggleClasses(editButton,"show", "hide");
  toggleClasses(doneButton,"show", "hide");
  return editContact(contenteditable);
}

export class ContactInfoComponent extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {

    return (
      <div id="contactinfo" style={{position: "relative"}}>
      <table id="table">
        <tbody>
          <tr>
            <th>
              <img src={this.props.activeContact.src} style={{borderColor: "#ddd",borderRadius: "50%", border:"2px solid #ddd", width: "80px", height: "80px"}}></img>
            </th>
            <th style={{float: "left",marginTop: "30px", fontSize: "20px", fontWeight: "normal"}}>{this.props.activeContact.name}</th>
          </tr>
          {
            (this.props.activeContact.main) &&
            <tr className="infoRow">
            <td>
              <span>main</span>
            </td>
            <td style={{color: "black"}}>{this.props.activeContact.main}</td>
            <td></td>
            </tr>
          }
          {
            (this.props.activeContact.mobile) &&
            <tr className="infoRow">
            <td>
              <span>mobile</span>
            </td>
            <td>{this.props.activeContact.mobile}</td>
            <td></td>
            </tr>
          }
          <tr className="infoRow">
            <td>
              <span>call</span>
            </td><td>FaceTime</td>
            <td>Audio</td>
          </tr>
          {
            (this.props.activeContact.home_page) &&
            <tr className="infoRow">
              <td>
                <span>home page</span>
              </td>
              <td>
                <a href={this.props.activeContact.home_page}>{this.props.activeContact.home_page}</a>
              </td>
              <td></td>
            </tr>
          }
          {
            (this.props.activeContact.work) &&
            <tr className="infoRow">
              <td>
                <span>work</span>
              </td>
              <td style={{color: "black"}}>{this.props.activeContact.work}</td>
              <td></td>
            </tr>
          }
          <tr className="infoRow">
            <td>
              <span>note</span>
            </td>
            <td>{this.props.activeContact.note}</td>
            <td></td>
          </tr>
        </tbody>
      </table>
      <EditContact editContact={this.props.onEditContact}/>
    </div>
    );
  }
}