import React from 'react';

export class SearchBar extends React.Component {
	constructor(props) {
		super(props);
		this.handleFilterText = this.handleFilterText.bind(this);
	}
	handleFilterText(e) {
		this.props.handleFilterText(e.target.value);
	}
	render () {
		return (
			<div className="search">
				<span className="fa fa-search"></span>
				<input type="search" id="searchbar" placeholder="Search" style={{textAlign: "center",width: "90%",marginTop: "5%",marginLeft: "5%"}} value={this.props.filterText} onChange={this.handleFilterText} />
			</div>
		);
	}
}