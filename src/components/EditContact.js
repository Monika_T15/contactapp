import React from 'react';
import { toggleClasses } from '../utils/toggle-classes';

export class EditContact extends React.Component {
	constructor (props) {
  	super(props);
    this.showOptions = this.showOptions.bind(this);
  }
  showOptions () {
    var content = document.getElementById("content");
    toggleClasses(content, "show");
  }
  render () {
  	return (
  		<div>
      	<div className="dropdown">
    			<div className="dropdown-content hide" id="content">
    	   		<span>New Contact</span>
    		    <span>New Group</span>
    			  <hr></hr>
    			  <span>Phone</span>
  			  </div>
  			 <button className="dropbtn" onClick={this.showOptions}>+</button>
			  </div>
      	<button style={{right: "5%", position: "absolute", bottom: "3%"}} onClick={this.props.editContact} id="edit" className="show">Edit</button>
    		<button style={{position: "absolute", bottom: "3%", right: "5%"}} onClick={this.props.editContact} id="done" className="hide">Done</button>
   		</div>
  		);
  	}
}