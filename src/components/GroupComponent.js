import React from 'react';
import { groupsArray } from '../constants/contact-app-data';
import { GroupSection } from './GroupSection';
import { changeGroup } from '../actions';

export class GroupComponent extends React.Component {
  constructor (props) {
    super(props);
  }

  render() {
    const listItems = groupsArray.map((groupSection) => {
      return (
          <GroupSection key={groupSection.id} groupSection={groupSection} activeGroup={this.props.activeGroup.group} handleGroup={this.props.onGroupChange}/>
      )
    });
    return (
      <div style={{backgroundColor: "rgb(245,245,245)",border: "2px"}} id="groupscontainer">
        {listItems}
      </div>
    );
  }
}

export function onGroupChange (group) {
  for (let i in groupsArray)
  {
    for (let j in groupsArray[i].groups) {
      if (groupsArray[i].groups[j].name === group)
        return changeGroup(groupsArray[i].groups[j]);
    }
  }
}