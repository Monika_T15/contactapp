import React from 'react';

export class GroupHeader extends React.Component {
  constructor (props) {
    super(props);
  }
  render() {
    return (
      <h5>{this.props.header}</h5>
    );
  }
}