import React from 'react';
import GroupContainer from '../containers/GroupContainer';
import ContactContainer from '../containers/ContactContainer';
import ContactInfoContainer from '../containers/ContactInfoContainer';

import { contactsArray, groupsArray } from '../constants/contact-app-data';

export class ContactsApp extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {
    return (
        <div id="maincontainer">
          <GroupContainer />
          <ContactContainer />
          <ContactInfoContainer />
        </div>
    );
  }
}