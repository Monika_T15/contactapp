import React from 'react';

export class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (<li className={this.props.class} onClick={this.props.handleChange.bind(this,this.props.item)}>{this.props.item}</li>);
  }
}