import React from 'react';
import {ListItem} from './ListItem';
import {GroupHeader} from './GroupHeader';

export class GroupSection extends React.Component {
  constructor (props) {
    super(props);
    this.handleChange = this.props.handleGroup.bind(this);
  }
  render() {
    const listItems = this.props.groupSection.groups.map((group) => {
      let active = (group.id === this.props.activeGroup)?"mainList active":"mainList";
      return <ListItem key={group.id} item={group.name} class={active} handleChange={this.handleChange}/>
    }
    );
    return (
      <section>
        <GroupHeader header={this.props.groupSection.header} />
        <ul>
          {listItems}
        </ul>
      </section>
    );
  }
}