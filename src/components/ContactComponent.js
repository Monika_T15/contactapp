import React from 'react';
import {SearchBar} from './SearchBar';

import { contactsArray } from '../constants/contact-app-data';
import { filterContacts } from '../utils/filter-contacts';

import { changeContact, filterContact } from '../actions';

export class ContactComponent extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {
    const listItems = filterContacts(this.props.activeGroup.group, this.props.activeContact, this.props.filterText, this.props.onContactChange);
    return (
      <div id="contactcontainer">
        <SearchBar handleFilterText={this.props.handleFilterText} filterText={this.props.filterText} />
        <ul>
          {listItems}
        </ul>
      </div>
    );
  }
}

export function handleFilterText (filterText) {
  return filterContact(filterText);
}

export function onContactChange (contact) {
  for (let i in contactsArray) {
    if (contactsArray[i].name === contact)
      return changeContact(contactsArray[i]);
  }
}