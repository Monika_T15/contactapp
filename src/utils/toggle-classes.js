module.exports = {
	toggleClasses: (element,...classes) => {
	    for (let i in classes) {
	      if (classes.hasOwnProperty(i))
	        element.classList.toggle(classes[i]);
	    }
  	}	
}