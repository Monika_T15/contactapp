import React from 'react';
import { contactsArray } from '../constants/contact-app-data';
import { filterContactsInGroup } from '../utils/filter-contacts-in-group';
import { ListItem } from '../components/ListItem';

module.exports = {
  filterContacts: (activeGroup, activeContact, filterText, handleContact) => {
    const filteredContactsInGroup = contactsArray.filter(filterContactsInGroup.bind(null,activeGroup));
    const listItems = filteredContactsInGroup.map((contact) => {
      let className = (contact.name === activeContact.name)?"contactList active":"contactList";
      let searchText = (filterText && filterText.toUpperCase)?filterText.toUpperCase():filterText;
      for (let key in contact) {
        if (contact.hasOwnProperty(key)) {
          let contactParam = (contact[key] && contact[key].toUpperCase)?contact[key].toUpperCase():contact[key];
          if (contactParam.indexOf(searchText) !== -1)
            return <ListItem key={contact.name} handleChange={handleContact} item={contact.name} class={className}/> ;
        }
      }
    });
    return listItems;
  }
}