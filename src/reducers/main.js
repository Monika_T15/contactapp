import { combineReducers } from 'redux';
import group from './Group';
import contact from './Contact';

const appReducers = combineReducers({
  group,
  contact
});

export default appReducers;
