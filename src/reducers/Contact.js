import { contactsArray } from '../constants/contact-app-data';

const initialState = {
	"activeContact": contactsArray[0], 
    "contactEditable": false,
    "filterText": ""
};

const contact = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE_CONTACT':
			return Object.assign({}, state, {
        		"activeContact": action.contact
      		})
		case 'EDIT_CONTACT':
			return Object.assign({}, state, {
        		"contactEditable": action.contactEditable
      		})
      	case 'FILTER_CONTACT':
      		return Object.assign({}, state, {
        		"filterText": action.filterText
      		})
		default:
			return state;
	}
}

export default contact;