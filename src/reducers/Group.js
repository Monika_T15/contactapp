import { groupsArray } from '../constants/contact-app-data';

const initialState = {
	"group": groupsArray[0].groups[0].id
};

const group = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE_GROUP':
			return Object.assign({}, state, {
        		"group": action.group
      		})
		default:
			return state;
	}
}

export default group;