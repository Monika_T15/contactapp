export const changeGroup = (group) => ({
  type: 'CHANGE_GROUP',
  group: group.id
});

export const changeContact = (contact) => ({
  type: 'CHANGE_CONTACT',
  contact: contact
});

export const editContact = (contactEditable) => ({
  type: 'EDIT_CONTACT',
  contactEditable: contactEditable
});

export const filterContact = (filterText) => ({
  type: 'FILTER_CONTACT',
  filterText: filterText
});