import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers/main';
import { ContactsApp } from './components/ContactsApp';

const store = createStore(reducer);

ReactDOM.render(
  (<Provider store={store}>
  	<ContactsApp />
  </Provider>),
  document.getElementById('root')
);
