import React from 'react';
import { connect } from 'react-redux';
import { GroupComponent,  onGroupChange} from '../components/GroupComponent';

const mapStateToProps = (state) => ({
  activeGroup: state.group
})

const mapDispatchToProps = {
  onGroupChange: onGroupChange
}

const GroupContainer = connect (
  mapStateToProps,
  mapDispatchToProps
)(GroupComponent);

export default GroupContainer;