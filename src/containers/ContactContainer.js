import React from 'react';
import { connect } from 'react-redux';
import { ContactComponent,  onContactChange, handleFilterText } from '../components/ContactComponent';

const mapStateToProps = (state) => ({
  activeContact: state.contact.activeContact,
  activeGroup: state.group,
  filterText: state.contact.filterText
})

const mapDispatchToProps = {
  onContactChange: onContactChange,
  handleFilterText: handleFilterText
}

const ContactContainer = connect (
  mapStateToProps,
  mapDispatchToProps
)(ContactComponent);

export default ContactContainer;