import React from 'react';
import { connect } from 'react-redux';
import { ContactInfoComponent,  onEditContact } from '../components/ContactInfoComponent';

const mapStateToProps = (state) => ({
  activeContact: state.contact.activeContact,
  contactEditable: state.contact.contactEditable
})

const mapDispatchToProps = {
  onEditContact: onEditContact
}

const ContactInfoContainer = connect (
  mapStateToProps,
  mapDispatchToProps
)(ContactInfoComponent);

export default ContactInfoContainer;