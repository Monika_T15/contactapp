module.exports = {
    entry: './src/main.jsx',
    output: {
        path: 'dist',
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: '/node_modules',
            loader: 'babel',
            query: {
                presets: ['es2015', 'react']
            }
        }]
    }
}